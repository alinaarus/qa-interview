import time  
# This is where the webdriver is imported
from selenium import webdriver                   
#  The ChromeDriver was imported here

from webdriver_manager.chrome import ChromeDriverManager
#  installation of ChromeDriver 
web=webdriver.Chrome(ChromeDriverManager().install())
web.maximize_window()

# The URL has been opened here
web.get("https://tickbird-dev.web.app")

# The "accept cookies" button has been pressed
AcceptCookies= web.find_element_by_xpath('/html/body/div[4]/div[2]/a')
AcceptCookies.click()
# I entered a 10-second pause to scroll
time.sleep(10)
# Here I started to fill in the form with my data
YourName = "Alina Rus"
Name = web.find_element_by_id('name')
Name.send_keys(YourName)

Email = "alina.rus0909@yahoo.com"
Mail = web.find_element_by_id('email')
Mail.send_keys(Email)

Phone = "0757733472"
phonenumber = web.find_element_by_id('phone')
phonenumber.send_keys(Phone)

Company = "TickBird"
CompanyName = web.find_element_by_id('company')
CompanyName.send_keys(Company)

Summary = "Hello! I would like to apply for a job."
RequestSummary = web.find_element_by_id('message')
RequestSummary.send_keys(Summary)

# The "agree to the terms and conditions" button is accepted here
PrivacyPolicy_btn = web.find_element_by_id('agreement')
PrivacyPolicy_btn.click()

# The form was submitted here"

Submit_form_btn = web.find_element_by_xpath('/html/body/div[2]/div[2]/div/div/div[1]/div[6]/div[1]/form/div/button')
Submit_form_btn.submit()



# Here is a function that checks if the "Thank you!"" message appears after the form was submitted"
get_confirmation_div_text = web.find_element_by_class_name('highlight')
print(get_confirmation_div_text.text)
if((get_confirmation_div_text.text) == "Thank you!"):
     print ("Test Was Successful")
else:
    print ("Test Was Not Successful")


