
from behave import AND, given, when, then
from FirstPython import run

@given(u'Browser is open')
def step_impl(context):
    print(u'STEP: Given- Browser is open')
    pass

@AND(u'Website is open')
def step_impl(context):
    print(u'STEP: And- Website is open')
    pass

@AND(u'user is at the "Contact Us" form')
def step_impl(context):
    print(u'STEP: And- user is at the "Contact Us" form')
    pass


@when(u' user enters valid name, email, phone, company, request summary and agrees with Terms and Conditions')
def step_impl(context):
    print(u'STEP: When user enters valid name, email, phone, company, request summary and agrees with Terms and Conditions') 


@when (u'user clicks on Submit button')
def step_impl(context):
    print(u'STEP: When- user clicks on Submit button')
    pass


@then(u'The "thank you!" message should be displayed.')
def step_impl(context, out):
    print(u'STEP: The "thank you!" message should be displayed.')
    assert context.result == format(out, context.result)
