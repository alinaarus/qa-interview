Feature: Test "Contact Us" form  Functionality
  Scenario: Check "thank you!" message appears after user submits the form
    Given browser is open
    And website is open
    And user is at the "Contact Us" form
    When user enters valid name, email, phone, company, request summary and agrees with Terms and Conditions
    When user clicks on Submit button
    Then The "thank you!" message should be displayed.

  


